# NodeJS PGH Meetup Site

[![LICENSE](https://img.shields.io/badge/license-MIT-lightgrey.svg)](https://github.com/h5bp/html5-boilerplate/blob/master/LICENSE.txt)

A site created for the NodePgh Meetup Group.
---

Production Site: https://node-website-production.herokuapp.com/

----

To Run:

`npm i`

`npm start`

----
Docker Instructions:

`docker pull ephemeralwaves/nodepghserver:1.2.0`

or

Build locally with:

`docker build -t nodepghserver-run -f Dockerfile-run .`

and run :
`docker run -i -p 3000:3000 -t nodepghserver-run`

----
