const express = require ('express');

const router = express.Router();
const feedbackRoute = require('./feedback');
const eventsRoute = require('./events');

module.exports = (param) => {
  router.get('/', (req,res,next) => {
    return res.render('index', {
      page: 'Home',
    });
  });
  router.use('/feedback',feedbackRoute(param));
  router.use('/events',eventsRoute(param));

  return router;
};
